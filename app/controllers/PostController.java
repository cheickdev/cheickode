package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import models.Post;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;

import views.html.*;
import views.html.posts.*;

import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class PostController extends Secure {

    static final String PLEASE_CONNECT_TO_EDIT_THIS_POST = "Please connect to edit this post";
    static final String PAGE_NOT_FOUND = "Page not found";
    @Inject
    private FormFactory factory;

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        List<Post> posts = Post.find.order().desc("modify_date").findList();
        return ok(index.render(posts));
    }

    public Result posts() {
        return ok();
    }

    public Result create() {
        return ok(views.html.posts.create.render(factory.form(Post.class)));
    }

    @Security.Authenticated(LoginAction.class)
    public Result edit(Long id) {
        if (connected() == null) {
            return unauthorized(unauthorized.render(PLEASE_CONNECT_TO_EDIT_THIS_POST));
        }
        Post post = Post.find.byId(id);
        if (post == null) {
            return notFound(PAGE_NOT_FOUND);
        }
        Form<Post> form = factory.form(Post.class);
        form = form.fill(post);
        return ok(views.html.posts.edit.render(form));
    }

    @Security.Authenticated(LoginAction.class)
    public Result post() {
        Form<Post> form = factory.form(Post.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.posts.create.render(form));
        }
        Post post = form.get();
        post.save();
        return redirect(routes.PostController.index());
    }

    public Result show(Long id) {
        Post post = Post.find.byId(id);
        if (post == null) {
            return notFound(PAGE_NOT_FOUND);
        }
        return ok(views.html.posts.post.render(post));
    }

    @Security.Authenticated(LoginAction.class)
    public Result update(Long id) {
        if (connected() == null) {
            return unauthorized(unauthorized.render(PLEASE_CONNECT_TO_EDIT_THIS_POST));
        }
        Post fromDatabase = Post.find.byId(id);
        if (fromDatabase == null) {
            return notFound(PAGE_NOT_FOUND);
        }
        Form<Post> form = factory.form(Post.class).bindFromRequest();
        if (form.hasErrors()) {
            return badRequest(views.html.posts.edit.render(form));
        }
        Post post = form.get();
        fromDatabase.setTitle(post.title);
        fromDatabase.setProblem(post.problem);
        fromDatabase.setSolution(post.solution);
        fromDatabase.save();
        return redirect(routes.PostController.show(id));
    }

    @Security.Authenticated(LoginAction.class)
    public Result delete(Long id) {
        if (connected() == null) {
            return unauthorized(unauthorized.render(PLEASE_CONNECT_TO_EDIT_THIS_POST));
        }
        Post post = Post.find.byId(id);
        if (post == null) {
            return notFound(PAGE_NOT_FOUND);
        }
        post.delete();
        ObjectNode result = Json.newObject();
        result
                .put("state", "success")
                .put("message", post.title + " Deleted")
        ;
        return ok(result);
    }

}
