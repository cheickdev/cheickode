package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import models.Article;
import models.Post;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.html.articles.index;
import views.html.unauthorized;

import java.util.List;

import static controllers.PostController.PAGE_NOT_FOUND;

/**
 * Created by sissoko on 31/08/2016 13:22.
 */
public class ArticleController extends Controller {

	@Inject
	private FormFactory factory;

	public Result create() {
		return ok(views.html.articles.create.render(factory.form(Article.class)));
	}

	@Security.Authenticated(LoginAction.class)
	public Result post() {
		Form<Article> form = factory.form(Article.class).bindFromRequest();
		System.out.println(form);
		if (form.hasErrors()) {
			return badRequest(views.html.articles.create.render(form));
		}
		Article post = form.get();
		post.save();
		return redirect(routes.ArticleController.index());
	}

	@Security.Authenticated(LoginAction.class)
	public Result edit(long id) {
		Article post = Article.find.byId(id);
		if (post == null) {
			return notFound(PAGE_NOT_FOUND);
		}
		Form<Article> form = factory.form(Article.class);
		form = form.fill(post);
		return ok(views.html.articles.edit.render(form));
	}

	public Result index() {
		List<Article> articles = Article.find.findList();
		return ok(index.render(articles));
	}

	@Security.Authenticated(LoginAction.class)
	public Result update(Long id) {
		Article fromDatabase = Article.find.byId(id);
		if (fromDatabase == null) {
			return notFound(PAGE_NOT_FOUND);
		}
		Form<Article> form = factory.form(Article.class).bindFromRequest();
		if (form.hasErrors()) {
			return badRequest(views.html.articles.edit.render(form));
		}
		Article article = form.get();
		fromDatabase.setTitle(article.getTitle());
		fromDatabase.setContent(article.getContent());
		fromDatabase.save();
		return redirect(routes.ArticleController.show(id));
	}

	public Result show(Long id) {
		Article article = Article.find.byId(id);
		if (article == null) {
			return notFound(PAGE_NOT_FOUND);
		}
		return ok(views.html.articles.article.render(article));
	}

	@Security.Authenticated(LoginAction.class)
	public Result delete(Long id) {
		Article article = Article.find.byId(id);
		if (article == null) {
			return notFound(PAGE_NOT_FOUND);
		}
		article.delete();
		ObjectNode result = Json.newObject();
		result
				.put("state", "success")
				.put("message", article.getTitle() + " Deleted")
		;
		return ok(result);
	}
}
