package controllers;

import models.User;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Security.Authenticator;

/**
 * Created by sissoko on 25/07/2016.
 */
public class LoginAction extends Authenticator {

	@Override
	public String getUsername(Http.Context ctx) {
		String username = super.getUsername(ctx);
		if (username == null) {
			return null;
		}
		if (User.find.byUsername(username) == null) {
			return null;
		}
		return username;
	}

	@Override
	public Result onUnauthorized(Http.Context ctx) {
		return unauthorized("You need to be connected");
	}

}
