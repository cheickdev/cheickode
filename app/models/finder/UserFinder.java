package models.finder;

import com.avaje.ebean.Model;
import models.User;

/**
 * Created by sissoko on 31/08/2016 14:34.
 */
public class UserFinder extends Model.Finder<Long, User> {

	public UserFinder() {
		super(User.class);
	}

	public User byUsername(String username) {
		return where().eq("email", username).findUnique();
	}

}
