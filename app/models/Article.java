package models;

import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * Created by sissoko on 31/08/2016 13:12.
 */
@Entity
@Table(name = "T_ARTICLES")
public class Article extends ModelBase {

	public static final Finder<Long, Article> find = new Finder<>(Article.class);
	@Id
	@Column(name = "ID")
	private Long id;

	@Constraints.Required(message = "Title is required")
	@Column(name = "TITLE")
	private String title;

	@Constraints.Required(message = "Content is required")
	@Column(name = "CONTENT")
	@Lob
	private String content;

	@ManyToOne
	@JoinColumn(name = "AUTHOR_ID")
	private User author;

	@Override
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public User getAuthor() {
		return author;
	}

	public void setAuthor(User author) {
		this.author = author;
	}
}
