#!/usr/bin/env python
import getopt, sys
import os
from subprocess import call

def usage():
    print "djammamodule.py [options]"
    print "    -h (--help)    : print this help"
    print "    -n (--new)     : Create new play project with djamma-play-quickstart"
    print "    -m (--modules) module1[,module2,module2...] : list module to load separated by ',' (Available : plugins, crud, secure)"
    print "    Example:"
    print "             djammamodule.py -m plugins,crud"
    print "             Will checkout DjammaPlugins and Crud module from git and add them to the current project."
    print "    NB : If you checkout a module (crud or secure) be sure to update build.sbt file in the project root by adding the line below to your project definition"
    print "    .dependsOne(modulename).aggregate(modulename)"

def clone(repos, name = ''):
    call(['git', 'clone', repos, name])

def get_module_from_git(module, dest):
    print "Checking out Djamma %s Module..." % module
    print "git clone git@bitbucket.org:djammadev/djamma%s.git" % module
    repos = "git@bitbucket.org:djammadev/djamma%s.git" % module
    clone(repos, 'djamma%s' % module)
    path = "%s/%s" % (dest, module)
    if not os.path.isdir(path):
        print "Creating %s..." % path
        os.makedirs(path)
    print path
    modname = "djamma%s" % module
    print "Moving %s/* --> %s" % (modname, path)
    os.system('cp -rf %s/* %s' % (modname, path))
    print "Removing temporary file %s..." % modname
    os.system('rm -rf %s' % modname)
    if module != "plugins":
        print "Updating build.sbt for %s module" % module
        deps = "lazy val %s: Project = (project in file(\\\"modules/%s\\\")).enablePlugins(PlayJava, PlayEbean).settings(aggregateReverseRoutes := Seq())" % (module, module)
        os.system('echo "%s" >> build.sbt' % deps)

def create_new_project_from_quickstart(project):
    print "Cloning djamma-play-quickstart project as %s" % project
    repos = "git@bitbucket.org:djammadev/djamma-play-quickstart.git"
    clone(repos, project)
    print "cd %s" % project
    os.chdir(project)
    sed ="sed -i '' s/\${projectName}/%s/g build.sbt" % project
    print sed
    call(['sed', '-i', '', 's/\${projectName}/%s/g' % project, 'build.sbt'])
    # os.system(sed)
    print "Update Application Secret"
    call(['activator', 'playUpdateSecret'])
    print "Remove old git"
    call(['rm', '-rf', '.git'])
    print "project %s created" % project

def handle_modules(modules):
    for module in modules:
        if module == 'crud':
            print "Adding crud module..."
            get_module_from_git(module, "modules")            
        if module == 'secure':
            print "Adding secure module..."
            get_module_from_git(module, "modules")
        if module == 'plugins':
            print "Adding plugins support..."
            get_module_from_git(module, "project")
            print "Updating plugins dependencies"
            os.system('cat project/plugins/plugins.sbt >> project/plugins.sbt')
	if module == 'oauth':
	    print "Adding oauth module..."
            get_module_from_git(module, "modules")


def main():
    try:
	    opts, args = getopt.getopt(sys.argv[1:], "hm:n:", ["help", "modules=", "new="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    modules=[]
    project = None
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-m", "--modules"):
            modules = a.split(',')
	elif o in ("-n", "--new"):
	    project = a
        else:
            assert False, "unhandled option"

    if project <> None:
        create_new_project_from_quickstart(project)
    handle_modules(modules)
if __name__ == "__main__":
    main()
