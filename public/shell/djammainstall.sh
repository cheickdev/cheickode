#!/bin/bash

if [ -z "$1" ]
then
	echo "Please give a play version to download"
	exit 0;
fi
activatorbin=`which activator`
VERSION="$1"
ACTIVATOR="activator-$VERSION-minimal"
FRAMEWORK_DIR="$HOME/Developer/frameworks"
ACTIVATOR_DIR="$FRAMEWORK_DIR/$ACTIVATOR"
ACTIVATOR_BIN="$ACTIVATOR_DIR/bin/activator"
DOWNLOAD_URL="https://downloads.typesafe.com/typesafe-activator/$VERSION/typesafe-$ACTIVATOR.zip"

echo "activator not installed."
echo "It will be installed under $HOME/Developer/frameworks/"
echo "Check for wget"
dldbin=`which wget`
if [ -z $dldbin ]
then
	echo "No downloder found"
	echo "please install wget and try again using"
	echo "sudo apt-get install wget"
	exit 0
fi	
$dldbin $DOWNLOAD_URL -O tmp.zip
if [ -f tmp.zip ]
then
	unzip -u tmp.zip -d $FRAMEWORK_DIR
fi
echo "ln -sF $ACTIVATOR_BIN $HOME/bin/activator"
ln -sF $ACTIVATOR_BIN $HOME/bin/activator
rm -f tmp.zip
#source ~/.profile
activatorbin=`which activator`
if [ -z $activatorbin ]
then
	echo "Activator installation failed"
	exit 1
fi
echo "Activator installation done."


